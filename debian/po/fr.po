# Translation of plinth debconf templates to French
# Copyright (C) 2018 FreedomBox packaging team <freedombox-pkg-team@lists.alioth.debian.org>
# This file is distributed under the same license as the plinth package.
#
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2018.
msgid ""
msgstr ""
"Project-Id-Version: plinth\n"
"Report-Msgid-Bugs-To: plinth@packages.debian.org\n"
"POT-Creation-Date: 2018-07-03 16:39+0530\n"
"PO-Revision-Date: 2018-07-30 23:19+0100\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr_FR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 2.0\n"

#. Type: note
#. Description
#: ../templates:1001
msgid "FreedomBox first wizard secret - ${secret}"
msgstr "Phrase secrète du premier assistant de FreedomBox - ${secret}"

#. Type: note
#. Description
#: ../templates:1001
msgid ""
"Please save this string. You will be asked to enter this in the first screen "
"after you launch the FreedomBox interface. In case you lose it, you can find "
"it in the file /var/lib/plinth/firstboot-wizard-secret."
msgstr ""
"Veuillez retenir cette chaîne. Le premier écran après le chargement de "
"l'interface de FreedomBox vous la demandera. Si vous l'avez oubliée, vous "
"pourrez la retrouver dans le fichier /var/lib/plinth/firstboot-wizard-secret."

